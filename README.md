## Go-Avatars
Generate beautiful avatars for your users based on their initials. To get started, clone the repository with:
```bash
$ git clone https://gitlab.com/idoko/go-avatars.git
```
Start the go server with:
```bash
$ go run ./main.go
```
In your browser, visit the avatar generator URL, passing in your initials
as query parameters e.g: <http://localhost:3000/avatar?initials=MO/>