package main

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"image/png"
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/golang/freetype"
	"github.com/golang/freetype/truetype"
	"golang.org/x/image/font"
	"golang.org/x/image/font/gofont/goregular"
	"golang.org/x/image/math/fixed"
)

func main() {
	router := chi.NewRouter()
	router.Use(middleware.Logger)
	router.Get("/avatar", func(w http.ResponseWriter, r *http.Request) {
		initials := r.FormValue("initials")
		size, err := strconv.Atoi(r.FormValue("size"))
		if err != nil {
			size = 200
		}
		avatar, err := createAvatar(size, initials)
		if err != nil {
			log.Fatal(err)
		}
		w.Header().Set("Content-Type", "image/png")
		png.Encode(w, avatar)
	})
	http.ListenAndServe(":3000", router)
}

func createAvatar(size int, initials string) (*image.RGBA, error) {
	width, height := size, size
	bgColor, err := hexToRGBA("#764abc")
	if err != nil {
		log.Fatal(err)
	}

	background := image.NewRGBA(image.Rect(0, 0, width, height))
	draw.Draw(background, background.Bounds(), &image.Uniform{C: bgColor},
		image.Point{}, draw.Src)

	drawText(background, initials)
	return background, err
}

func drawText(canvas *image.RGBA, text string) error {
	var (
		fgColor  image.Image
		fontFace *truetype.Font
		err      error

		fontSize = 128.0
	)
	fgColor = image.White
	fontFace, err = freetype.ParseFont(goregular.TTF)
	fontDrawer := &font.Drawer{
		Dst: canvas,
		Src: fgColor,
		Face: truetype.NewFace(fontFace, &truetype.Options{
			Size:    fontSize,
			Hinting: font.HintingFull,
		}),
	}
	textBounds, _ := fontDrawer.BoundString(text)
	xPosition := (fixed.I(canvas.Rect.Max.X) - fontDrawer.MeasureString(text)) / 2
	textHeight := textBounds.Max.Y - textBounds.Min.Y
	yPosition := fixed.I((canvas.Rect.Max.Y)-textHeight.Ceil())/2 + fixed.I(textHeight.Ceil())
	fontDrawer.Dot = fixed.Point26_6{
		X: xPosition,
		Y: yPosition,
	}
	fontDrawer.DrawString(text)
	return err
}

func hexToRGBA(hex string) (color.RGBA, error) {
	var (
		rgba color.RGBA
		err  error
	)
	rgba.A = 0xFF
	switch len(hex) {
	case 7:
		_, err = fmt.Sscanf(hex, "#%02x%02x%02x", &rgba.R, &rgba.G, &rgba.B)
	case 4:
		_, err = fmt.Sscanf(hex, "#%1x%1x%1x", &rgba.R, &rgba.G, &rgba.B)
		rgba.R *= 17
		rgba.G *= 17
		rgba.B *= 17
	default:
		err = fmt.Errorf("invalid hex code")
	}
	return rgba, err
}
