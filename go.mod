module gitlab.com/idoko/go-avatars

go 1.15

require (
	github.com/go-chi/chi v4.1.2+incompatible // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	golang.org/x/image v0.0.0-20200801110659-972c09e46d76 // indirect
)
